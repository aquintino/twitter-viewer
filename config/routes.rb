Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :hashtags, only: %w[index create destroy]
      resources :tweets, only: %w[index] do
        collection do
          post :get_updates
        end
      end
    end
  end
  root 'main#index'
  get '/*path' => 'main#index'
end
