require 'test_helper'

class TweetServiceTest < ActiveSupport::TestCase
  test "#monitored_tweets should return all tweets with monitored hashtags" do
    tweet_service = TweetService.new
    assert_equal tweet_service.monitored_tweets.length, 4
  end

  test "#create should create tweet with given attributes and attach given hashtags" do
    tweet_service = TweetService.new
    attributes = {
      text: "Test #rails",
      author: "aquintino",
      published_at: Time.now,
      external_id: 123,
      hashtags: [
        hashtags(:rails)
      ]
    }
    tweet = tweet_service.create(attributes)
    assert_equal tweet.text, attributes[:text]
    assert_equal tweet.author, attributes[:author]
    assert_equal tweet.published_at, attributes[:published_at]
    assert_equal tweet.external_id, attributes[:external_id]
    assert_equal tweet.hashtags, attributes[:hashtags]
  end

  test "#create should return existing tweet if external_id is already created" do
    postgres_test = tweets(:postgres_test)
    tweet_service = TweetService.new
    attributes = {
      text: "Test #rails",
      author: "aquintino",
      published_at: Time.now,
      external_id: postgres_test.external_id
    }
    tweet = tweet_service.create(attributes)
    assert_equal postgres_test, tweet
  end
end
