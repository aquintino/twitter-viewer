require 'test_helper'

class HashtagServiceTest < ActiveSupport::TestCase
  test "#find should find a hashtag by id" do
    hashtag_service = HashtagService.new
    ruby = hashtags(:ruby)
    assert_equal hashtag_service.find(ruby.id), ruby
  end

  test "#monitored_hashtags should return all monitored hashtags" do
    hashtag_service = HashtagService.new
    ruby = hashtags(:ruby)
    rails = hashtags(:rails)
    monitored_hashtags = hashtag_service.monitored_hashtags
    assert_includes monitored_hashtags, ruby
    assert_includes monitored_hashtags, rails
    assert_equal monitored_hashtags.length, 2
  end

  test "#monitored_hashtags should return only monitored hashtags" do
    hashtag_service = HashtagService.new
    node = hashtags(:node)
    monitored_hashtags = hashtag_service.monitored_hashtags
    assert_not_includes monitored_hashtags, node
  end

  test "#destroy should disable hashtag monitoring" do
    hashtag_service = HashtagService.new
    ruby = hashtags(:ruby)
    hashtag_service.destroy(ruby)
    destroyed_ruby = hashtags(:ruby)
    assert_not destroyed_ruby.monitor
  end

  test "#create should return an existing hashtag" do
    hashtag_service = HashtagService.new
    ruby = hashtags(:ruby)
    assert_equal hashtag_service.create("ruby"), ruby
  end

  test "#create should not update an existing hashtag with the provided attributes" do
    hashtag_service = HashtagService.new
    attributes = { monitor: false }
    ruby = hashtag_service.create("ruby", attributes)
    assert_not_equal ruby.monitor, attributes[:monitor]
  end

  test "#create should create a non existing hashtag with the provided attributes" do
    hashtag_service = HashtagService.new
    attributes = { monitor: false }
    mysql = hashtag_service.create("mysql", attributes)
    assert_equal mysql.monitor, attributes[:monitor]
  end

  test "#create_or_update should update an existing hashtag with the provided attributes" do
    hashtag_service = HashtagService.new
    attributes = { monitor: false }
    ruby = hashtag_service.create_or_update("ruby", attributes)
    assert_equal ruby.monitor, attributes[:monitor]
  end

  test "#mass_create should create all non existing hashtags with the provided attributes" do
    hashtag_service = HashtagService.new
    names = %w[rails ruby mysql react]
    tags = hashtag_service.mass_create(names, monitor: false)
    assert_equal [true, true, false, false], tags.pluck(:monitor)
  end
end
