class AddExternalIdToTweet < ActiveRecord::Migration[6.0]
  def change
    add_column :tweets, :external_id, :bigint, null: false, unique: true
  end
end
