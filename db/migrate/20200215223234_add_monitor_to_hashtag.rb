class AddMonitorToHashtag < ActiveRecord::Migration[6.0]
  def change
    add_column :hashtags, :monitor, :boolean, default: true
  end
end
