hashtag_service ||= HashtagService.new

%w[ruby rails react node nodejs postgres].each do |tag|
  hashtag = hashtag_service.create_or_update(tag, monitor: true)
  hashtag_service.populate_tweets_for(hashtag)
end
