json.array! @tweets do |tweet|
  json.extract! tweet, :id, :text, :author, :published_at
  json.hashtags tweet.hashtags.pluck(:name)
end