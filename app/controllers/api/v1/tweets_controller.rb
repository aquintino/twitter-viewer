class Api::V1::TweetsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @tweets = tweet_service.monitored_tweets
  end

  def get_updates
    @tweets = hashtag_service.monitor
    render :index
  end

  private

  def tweet_service
    @tweet_service ||= TweetService.new
  end

  def hashtag_service
    @hashtag_service ||= HashtagService.new
  end
end
