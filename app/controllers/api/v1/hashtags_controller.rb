class Api::V1::HashtagsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    render json: hashtag_service.monitored_hashtags
  end

  def create
    hashtag = hashtag_service.create_or_update(hashtag_params[:name], hashtag_params)
    if hashtag.persisted?
      tweet_service.populate_tweets_for(hashtag.name)
      render json: hashtag
    else
      render json: hashtag.errors
    end
  end

  def destroy
    hashtag_service.destroy(current_hashtag)
    render json: { message: 'Hashtag deleted!' }
  end

  private

  def tweet_service
    @tweet_service ||= TweetService.new
  end

  def hashtag_service
    @hashtag_service ||= HashtagService.new
  end

  def current_hashtag
    @current_hashtag ||= hashtag_service.find(params[:id])
  end

  def hashtag_params
    params.permit(:name, :monitor)
  end
end
