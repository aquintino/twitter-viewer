class TweetService
  def monitored_tweets
    tweet_ids = Tweet.joins(:hashtags).where(hashtags: { monitor: true }).pluck(:id)
    Tweet.includes(:hashtags).where(id: tweet_ids).order(published_at: :desc)
  end

  def create(text:, author:, published_at:, external_id:, hashtags: nil)
    tweet = Tweet.find_or_initialize_by(external_id: external_id)
    return tweet if tweet.persisted?
    tweet.assign_attributes(text: text, author: author, published_at: published_at)
    tweet.hashtags = hashtags if hashtags
    tweet.save!
    tweet
  end

  def monitor
    hashtag_service.monitored_hashtags.map do |hashtag|
      populate_new_tweets_for(hashtag)
    end.flatten.uniq
  end

  def populate_new_tweets_for(hashtag)
    since_id = hashtag.tweets.maximum(:external_id)
    tweets = twitter_service.tweets_by_hashtag(hashtag.name, since_id: since_id)
    tweet_service.populate_tweets(hashtag, tweets)
  end

  def populate_tweets_for(hashtag)
    tweets = twitter_service.tweets_by_hashtag(hashtag, limit: 100)
    populate_tweets(tweets)
  end

  def populate_tweets(tweets)
    all_hashtags = tweets.map do |tweet|
      tweet.hashtags.map(&:text)
    end.flatten.uniq
    mass_create(all_hashtags, monitor: false)
    tweets.map do |tweet|
      create(
        text: tweet.text,
        author: tweet.user.screen_name,
        published_at: tweet.created_at,
        external_id: tweet.id,
        hashtags: load_many(tweet.hashtags.map(&:text))
      )
    end
  end

  private

  def hashtag_service
    @hashtag_service ||= HashtagService.new
  end
end