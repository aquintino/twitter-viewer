class TwitterService
  def tweets_by_hashtag(hashtags, options = {})
    query = search_query(hashtags: hashtags)
    search(query, options)
  end

  private

  def search(query, result_type: "recent", include_entities: true, limit: nil, since_id: nil)
    query = client.search(
      query,
      result_type: result_type,
      include_entities: true,
      since_id: since_id
    )
    query = query.first(limit) if limit
    query
  end

  def search_query(hashtags: [], rt: false)
    hashtags = [hashtags] unless hashtags.is_a?(Array)
    [
      hashtags.map { |tag| "##{tag}" },
      rt ? nil : "-rt"
    ].flatten.compact.join(" ")
  end

  def client
    @client ||= begin
      twitter = Rails.application.credentials.twitter

      Twitter::REST::Client.new do |config|
        config.consumer_key        = twitter[:consumer_key]
        config.consumer_secret     = twitter[:consumer_secret]
        config.bearer_token        = twitter[:bearer_token]
        config.access_token        = twitter[:access_token]
        config.access_token_secret = twitter[:access_token_secret]
      end
    end
  end
end
