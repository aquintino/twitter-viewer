class HashtagService
  def find(id)
    Hashtag.find(id)
  end

  def monitored_hashtags
    Hashtag.where(monitor: true).order(:name)
  end

  def destroy(hashtag)
    hashtag.update(monitor: false)
    hashtag
  end

  def create(name, attributes = nil)
    hashtag = Hashtag.find_or_initialize_by(name: name.to_s.downcase)
    return hashtag if hashtag.persisted?
    hashtag.assign_attributes(attributes) if attributes
    hashtag.save
    cache(hashtag)
    hashtag
  end

  def create_or_update(name, attributes)
    hashtag = create(name, attributes)
    hashtag.assign_attributes(attributes) if attributes
    hashtag.save
    cache(hashtag)
    hashtag
  end

  def mass_create(names, attributes = nil)
    names = names.map { |name| name.to_s.downcase }
    hashtags = load_many(names)
    return hashtags if hashtags.size == names.size
    missing_hashtags = names - hashtags.pluck(:name)
    missing_hashtags.each do |name|
      hashtags << create(name, attributes)
    end
    hashtags
  end

  private

  def load(name)
    name = name.downcase
    cache[name] ||= begin
      Hashtag.find_by(name: name)
    end
  end

  def load_many(names)
    names = names.map(&:downcase) - cache.slice(*names).keys
    if (names.length > 0)
      tags = Hashtag.where(name: names)
      cache.merge!(tags.index_by(&:name))
    end
    cache.slice(*names).values
  end

  def cache(hashtag = nil)
    @cache ||= {}
    @cache[hashtag.name] = hashtag if hashtag
    @cache
  end

  def tweet_service
    @tweet_service ||= TweetService.new
  end

  def twitter_service
    @twitter_service ||= TwitterService.new
  end
end