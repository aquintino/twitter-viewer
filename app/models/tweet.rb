class Tweet < ApplicationRecord
  has_many :tweet_hashtags
  has_many :hashtags, through: :tweet_hashtags
end
