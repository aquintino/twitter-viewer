class Hashtag < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :tweet_hashtags
  has_many :tweets, through: :tweet_hashtags
end
