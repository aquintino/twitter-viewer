import React from "react";

export default () => (
  <div className="row">
    <div className="col-md-12">
      <div className="page-header">
        <h1>
          Twitter Viewer <small>Acompanhe as suas hashtags favoritas!</small>
        </h1>
      </div>
    </div>
  </div>
)