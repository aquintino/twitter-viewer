import React from "react";

import Header from './layout/Header'
import TweetList from './tweets/TweetList'
import AddHashtagForm from './hashtags/AddHashtagForm'
import HashtagList from './hashtags/HashtagList'

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hashtags: [],
      tweets: [],
      showingHashtags: []
    }
  }

  componentDidMount() {
    this.loadHashtags()
    this.loadTweets()
    this.loadShowingHashtags()
  }

  storeShowingHashtags() {
    localStorage.setItem("showingHashtags", JSON.stringify(this.state.showingHashtags))
  }

  loadShowingHashtags() {
    const showingHashtags = localStorage.getItem("showingHashtags")
    this.setState({
      showingHashtags: JSON.parse(showingHashtags) || []
    })
  }

  loadHashtags() {
    const url = "/api/v1/hashtags";
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Request failed.");
      })
      .then(response => this.setState({
        hashtags: response
      }))
      .catch(() => console.error('Request failed'));
  }

  loadTweets() {
    const url = "/api/v1/tweets";
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Request failed.");
      })
      .then(response => this.setState({ tweets: response }))
      .catch(() => console.error('Request failed'));
  }

  getUpdates() {
    const url = "/api/v1/tweets/get_updates";
    fetch(url, {
      method: "POST"
    }).then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Request failed.");
      })
      .then(response => this.setState({ tweets: this.state.tweets.concat(response) }))
      .catch(() => console.error('Request failed'));
  }

  addHashtag(hashtag) {
    if (this.state.hashtags.find(current_hashtag => current_hashtag.name === hashtag)) {
      return alert('Você já acompanha essa hashtag.')
    }
    const url = "/api/v1/hashtags/";
    fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: hashtag,
        monitor: true
      })
    }).then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Request failed.");
      })
      .then((response) => this.setState({
        hashtags: this.state.hashtags.concat([response]),
        showingHashtags: this.state.showingHashtags.concat([response.name])
      }, () => this.storeShowingHashtags()))
      .then(() => {
        this.loadTweets()
      })
      .catch(() => console.error('Request failed'));
  }

  destroyHashtag(deleted_hashtag) {
    const url = `/api/v1/hashtags/${deleted_hashtag.id}`;
    fetch(url, {
      method: "DELETE"
    }).then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Request failed.");
      })
      .then(() => this.setState({
        hashtags: this.state.hashtags.filter(hashtag => hashtag.id !== deleted_hashtag.id),
        showingHashtags: this.state.showingHashtags.filter(hashtag => hashtag !== deleted_hashtag.name)
      }, () => this.storeShowingHashtags()))
      .catch(() => console.error('Request failed'));
  }

  filteredTweets() {
    return this.state.tweets.filter(tweet => this.filterTweet(tweet)).sort((a, b) => a > b)
  }

  filterTweet(tweet) {
    return tweet.hashtags.filter(value => -1 !== this.state.showingHashtags.indexOf(value)).length > 0
  }

  isHashtagHidden(hashtag) {
    return !this.state.showingHashtags.includes(hashtag)
  }

  toggleHashtag(hashtagToggled) {
    if (!this.isHashtagHidden(hashtagToggled)) {
      this.setState({
        showingHashtags: this.state.showingHashtags.filter(hashtag => hashtag !== hashtagToggled)
      }, () => this.storeShowingHashtags())
    } else {
      this.setState({
        showingHashtags: this.state.showingHashtags.concat([hashtagToggled])
      }, () => this.storeShowingHashtags())
    }
  }

  render() {
    return (
      <div className="container-fluid">
        <Header />
        <div className="row">
          <div className="col-md-8">
            <TweetList tweets={this.filteredTweets()} getUpdates={this.getUpdates.bind(this)} />
          </div>
          <div className="col-md-4">
            <AddHashtagForm
              addHashtag={this.addHashtag.bind(this)}
            />
            <br/>
            <HashtagList
              hashtags={this.state.hashtags}
              showingHashtags={this.state.showingHashtags}
              toggleHashtag={this.toggleHashtag.bind(this)}
              isHashtagHidden={this.isHashtagHidden.bind(this)}
              destroyHashtag={this.destroyHashtag.bind(this)}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default Home