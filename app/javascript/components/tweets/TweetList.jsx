import React from "react";

import TweetCard from './TweetCard'

const tweet_card = (tweet) => (
  <TweetCard
    key={tweet.id}
    tweet={tweet}
  />
)

const tweet_list = (tweets) => tweets.map((tweet) => tweet_card(tweet))

export default ({tweets, getUpdates}) => (
  <div className="card">
    <div className="card-header">
      Tweets
      <div className="float-right">
        <button className="btn btn-success" onClick={() => getUpdates()}>
          Atualizar
        </button>
      </div>
    </div>
    <ul className="list-group list-group-flush">
      {tweet_list(tweets)}
    </ul>
  </div>
)