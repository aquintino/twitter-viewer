import React from "react";
import moment from 'moment';

const hashtag_url = "https://twitter.com/hashtag/"

const hashtag_link = (hashtag) => (
  <a
    key={hashtag}
    href={`${hashtag_url}${hashtag}`}
    style={{marginRight: '5px'}}
    target="_blank"
  >
    #{hashtag}
  </a>
)

const author_url = "https://twitter.com/"
const author_link = (author) => (
  <a
    key={author}
    href={`${author_url}${author}`}
    style={{marginRight: '5px'}}
    target="_blank"
  >
    @{author}
  </a>
)

const hashtag_list = (hashtags = []) => hashtags.map((hashtag) => hashtag_link(hashtag))

const localized_published_at = (tweet) => (moment(tweet.published_at).format("LLLL"))

export default ({tweet}) => (
  <div className="card" style={{margin: '5px'}}>
    <h5 className="card-header">
      {author_link(tweet.author)}
      <span className='float-right'>
        {localized_published_at(tweet)}
      </span>
    </h5>
    <div className="card-body">
      <p className="card-text">
        {tweet.text}
      </p>
    </div>
    <div className="card-footer">
      {hashtag_list(tweet.hashtags)}
    </div>
  </div>
)
