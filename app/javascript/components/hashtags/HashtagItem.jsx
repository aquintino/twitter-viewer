import React from "react";

export default ({hashtag, destroyHashtag, toggleHashtag, isHidden}) => (
  <li className="list-group-item">
    <div className="input-group">
      <span className={`form-control ${isHidden ? 'alert-secondary' : 'alert-success'}`}>
        #{hashtag.name}
      </span>
      <div className="input-group-append">
        <button className={`btn ${isHidden ? 'btn-outline-secondary' : 'btn-outline-success'}`} type="button" onClick={() => toggleHashtag(hashtag.name)}>{isHidden ? 'Mostrar' : 'Esconder'}</button>
        <button className="btn btn-outline-danger" type="button" onClick={() => destroyHashtag(hashtag)}>Remover</button>
      </div>
    </div>
  </li>
)