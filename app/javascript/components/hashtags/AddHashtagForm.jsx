import React from "react";


class AddHashtagForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hashtag: ""
    }
  }

  onHashtagChange(e) {
    this.setState({
      hashtag: e.target.value
    })
  }

  onSubmitForm(e) {
    e.preventDefault()
    this.props.addHashtag(this.state.hashtag)
    this.setState({
      hashtag: ""
    })
    return false
  }

  render () {
    return (
      <form role="form" onSubmit={this.onSubmitForm.bind(this)}>
        <div className="input-group">
          <div className="input-group-prepend">
            <label className="input-group-text">#</label>
          </div>
          <input type="text" className="form-control" value={this.state.hashtag} onChange={this.onHashtagChange.bind(this)} required={true} placeholder="Acompanhar hashtag..."/>
          <div className="input-group-append">
            <button className="btn btn-primary" type="submit">Adicionar</button>
          </div>
        </div>
      </form>
    )
  }
}

export default AddHashtagForm