import React from "react";

import HashtagItem from './HashtagItem'

class HashtagList extends React.Component {

  hashtag_item(hashtag) {
    return (
      <HashtagItem
        key={hashtag.id}
        hashtag={hashtag}
        toggleHashtag={this.props.toggleHashtag}
        destroyHashtag={this.props.destroyHashtag}
        isHidden={this.props.isHashtagHidden(hashtag.name)}
      />
    )
  }

  hashtag_list () {
    return this.props.hashtags.map((hashtag) => this.hashtag_item(hashtag))
  }

  render() {
    return (
      <div className="card">
        <div className="card-header">
          Hashtags
        </div>
        <ul className="list-group list-group-flush">
          {this.hashtag_list()}
        </ul>
      </div>
    )
  }
}

export default HashtagList