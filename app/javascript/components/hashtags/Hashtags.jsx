import React from "react";
import { Link } from "react-router-dom";

class Hashtags extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hashtags: []
    };
  }

  componentDidMount() {
    const url = "/api/v1/hashtags";
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then(response => this.setState({ hashtags: response }))
      .catch(() => this.props.history.push("/"));
  }
  render() {
    const { hashtags } = this.state;
    const allHashtags = hashtags.map((hashtag, index) => (
      <div key={index} className="col-md-6 col-lg-4">
        <div className="card mb-4">
          <img
            src={hashtag.image}
            className="card-img-top"
            alt={`${hashtag.name} image`}
          />
          <div className="card-body">
            <h5 className="card-title">{hashtag.name}</h5>
            <Link to={`https://twitter.com/hashtag/${hashtag.name}`} className="btn custom-button">
              View Hashtag
            </Link>
          </div>
        </div>
      </div>
    ));
    const noHashtag = (
      <div className="vw-100 vh-50 d-flex align-items-center justify-content-center">
        <h4>
          No hashtags yet. Why not <Link to="/new_hashtag">create one</Link>
        </h4>
      </div>
    );

    return (
      <>
        <section className="jumbotron jumbotron-fluid text-center">
          <div className="container py-5">
            <h1 className="display-4">Hashtags for every occasion</h1>
            <p className="lead text-muted">
              We’ve pulled together our most popular hashtags, our latest
              additions, and our editor’s picks, so there’s sure to be something
              tempting for you to try.
            </p>
          </div>
        </section>
        <div className="py-5">
          <main className="container">
            <div className="text-right mb-3">
              <Link to="/hashtag" className="btn custom-button">
                Create New Hashtag
              </Link>
            </div>
            <div className="row">
              {hashtags.length > 0 ? allHashtags : noHashtag}
            </div>
            <Link to="/" className="btn btn-link">
              Home
            </Link>
          </main>
        </div>
      </>
    );
  }
}
export default Hashtags;