import React from "react";
import { render } from "react-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import Main from "../components/Main";
import moment from 'moment';

document.addEventListener("DOMContentLoaded", () => {
  moment.locale('pt-br');
  render(
    <Main />,
    document.body.appendChild(document.createElement("div"))
  );
});